/*
uses gulp modules: gulp gulp-sass gulp-sourcemaps gulp-pug gulp-clean-css gulp-concat-css
*/ 
'use strict';

var path = {
    input: {
        sass: "./src/sass/**/*.scss",
        js: "./src/js/**/*.js",
        pug: "./src/**/*.pug"
    },
    output: {
        css: "./build/css",
        js: "./build/js",
        html: "./build"
    }
}

var gulp = require('gulp');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var pug = require('gulp-pug');
var cleanCSS = require('gulp-clean-css');
var concatCss = require('gulp-concat-css');

gulp.task('sass', function(){
    return gulp.src(path.input.sass)
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(concatCss("/min/min.style.css"))
    .pipe(cleanCSS({compatibility: 'ie:8'}))
    .pipe(sourcemaps.write('./maps'))
    .pipe(gulp.dest(path.output.css))
});
gulp.task('sass:watch', function(){
    gulp.watch(path.input.sass, ['sass']);
});

// gulp.task("css", function(){
//     return gulp.src(path.output.css + "/**/*.css")
//     .pipe(sourcemaps.init())
//     .pipe(cleanCSS({compatibility: 'ie:8'}))
//     .pipe(concatCss("/min/min.style.css"))
//     .pipe(sourcemaps.write('./maps'))
//     .pipe(gulp.dest(path.output.css + "/min"))
// });

gulp.task('pug', function buildHTML() {
    return gulp.src(path.input.pug)
    .pipe(pug())
    .pipe(gulp.dest(path.output.html))
  });
gulp.task('pug:watch', function(){
    gulp.watch(path.input.pug, ['pug']);
});